import org.tanukisoftware.wrapper.WrapperSimpleApp;

/**
 * 用于打包启动
 * 
 * @author mango
 *
 */
public class ACatService extends WrapperSimpleApp {

	protected ACatService(String[] strings) {
		super(new String[] { strings[0], "productModel" });
	}

	public static void main(String args[]) {
		new ACatService(args);
	}
}
