package com.mango.acat.core.store;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * 类似session中的空间存储
 * @author meigang
 *
 */
public class SessionStore extends IStore{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Map<String,Object> data = Maps.newHashMap();
	
	public static void setAttr(String key,Object value){
		data.put(key, value);
	}
	
	public static Object getAttr(String key){
		return data.get(key);
	}
	
	public static Map<String,Object> getData(){
		return data;
	}
	
	public static void remove(String key){
		data.remove(key);
	}
	
	public static void clear(){
		data.clear();
	}
}
