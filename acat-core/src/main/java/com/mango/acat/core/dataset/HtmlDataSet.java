package com.mango.acat.core.dataset;

public class HtmlDataSet extends DataSet{
	private String path;
	
	public HtmlDataSet(String path) {
		this.setType(HTML);
		this.setPath(path);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
