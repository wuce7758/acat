package com.mango.acat.core.dataset;

import java.util.Map;

public class JsonDataSet extends DataSet{
	
	public JsonDataSet(Map<String,Object> data) {
		this.setType(JSON);
		this.setData(data);
	}
	
	private Map<String,Object> data;

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
	
}
