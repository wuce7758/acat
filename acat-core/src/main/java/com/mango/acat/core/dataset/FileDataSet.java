package com.mango.acat.core.dataset;

public class FileDataSet extends DataSet{
	
	public FileDataSet(String path){
		this.setType(FILE);
		this.setPath(path);
	}
	
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
}
