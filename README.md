#acat

基于netty打造的一个http java web服务框架。<br/>
（因为netty的异步io机制，能轻松满足一般的并发性能。）<br/>
利用活跃在开源社区中的工具集，快速搭建一款具有学习价值的框架。

1.实现mvc。
2.实现orm,数据库持久化。
3.实现ioc,简单依赖注入。
4.实现缓存集成。
5.实现分布式服务。
。。。

## 项目结构
* acat-parent - 父级别项目
* acat-core   - 核心项目，netty的http服务器和一些集成工具
* acat-web    - 界面代码，其实就是利用acat的底层来实现自己的业务逻辑。
* acat-web下的home目录下，有一个bin目录，其中是放的wrapper的脚步和运行环境。

## 依赖包
* netty - 能快速实现http服务器
* fastjson - alibaba的json包
* hutool - java工具集
* guava - google的Java工具包
* log4j - 日志包
* okhttp - http的工具包，比apache-common-net好用
* freemarker - Java模板引擎
* wrapper - java wrapper service将多个class的文件打包运行

## 快速运行
* 1.开发环境eclipse中运行，运行根目录下的Start.java的main方法。
* 2.服务器环境运行，将acat-web打包war包后解压，运行bin下的./start start即可。

## 2017-04-28
* 去掉acat-mve这一模块，移动内容到acat-core中。
* 基本实现session-cookie的功能（永久，未实现扩展属性，如有效期等）。
* 重新测试通过wrapper service在windows上。

## 2017-04-26

* 调整mvc的跳转部分到BaseAction中，并完成json的data参数。
* 实现requestStore和sessionStore的空间存储map,结合实现BaseAction的setAttr,getAttr和setSessionAttr,getSessionAttr。
* 优化初始化启动acat的输出日志。

<img src="./rs/images/out.png"/>
	   

## 2017-03-26

* 实现netty的http服务器方式。
* 实现请求和响应的类型定义，DataSet,ParaSet和其子类。
* 实现html，js,css,jpg等静态资源请求处理。
* 实现get,post请求的参数获取，及页面和json数据的返回。
* 实现静态文件下载，支持浏览器能支持预览的图片文件。
* 实现freemarker模板的集成。
* 实现wrapper的打包运行，测试mac(unix),windows通过。
* 将nettyDemo程序，整理到acat的maven项目中。

> 首页截图

<img src="./rs/images/index.png"/>
